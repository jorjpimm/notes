
function EXT_COLOR() { echo -ne "\[\033[38;5;$1m\]"; }

NO_COLOUR="\[\033[0m\]"
export PS1="[$(EXT_COLOR 202)\w${NO_COLOUR}] \$(git-radar --bash --fetch)\n$ "
